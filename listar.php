<!DOCTYPE html>
<html>
	<head>
		<title>Listando</title>
	</head>
	
	<body>
		<?php if( $_POST["event_list"] === "Equipamentos" ): ?>
			<h1>Listando os equipamentos</h1>
			<?php listar_equipamentos(); ?>
		<?php elseif( $_POST["event_list"] === "Locais" ): ?>
			<h1>Listando os Locais</h1>
			<?php listar_locais(); ?>
		<?php elseif( $_POST["event_list"] === "Responsáveis" ): ?>
			<h1>Listando os Responsáveis</h1>
			<?php listar_responsaveis(); ?>
		<?php else: ?>
			<?php header('Location: index.php'); ?>
		<?php endif; ?>
		
		<p><a href="index.php">Voltar</a></p>
	</body>
</html>

<?php
	function listar_responsaveis(){
		require_once "init_emufc.php";
		
		$sql = "SELECT * FROM responsaveis";
		$stmt = $ePDO->prepare($sql);
		$stmt->execute();
		$result = $stmt->FetchAll(PDO::FETCH_ASSOC);
		
		$rows = count( $result );
		for( $i=0; $i<$rows; $i++ ){
			echo "ID do Responsável: " . $result[$i]["resp_id"] . "<br>";
			echo "Nome: " . $result[$i]["nome"] . "<br>";
			echo "E-Mail: " . $result[$i]["email"] . "<br>";
			echo "Telefone: " . $result[$i]["telefone"] . "<br><br>";
		}
	}

	function listar_locais(){
		require_once "init_emufc.php";
		
		$sql = "SELECT * FROM locais";
		$stmt = $ePDO->prepare($sql);
		$stmt->execute();
		$result = $stmt->FetchAll(PDO::FETCH_ASSOC);
		
		$rows = count( $result );
		for( $i=0; $i<$rows; $i++ ){
			echo "ID do Local: " . $result[$i]["local_id"] . "<br>";
			echo "Nome: " . $result[$i]["nome"] . "<br>";
			echo "Latitude: " . $result[$i]["latitude"] . "<br>";
			echo "Longitude: " . $result[$i]["longitude"] . "<br><br>";
		}
	}

	function listar_equipamentos(){
		require_once "init_emufc.php";
		
		$sql = "SELECT * FROM equipamentos";
		$stmt = $ePDO->prepare($sql);
		$stmt->execute();
		$result = $stmt->FetchAll(PDO::FETCH_ASSOC);
		
		$rows = count( $result );
		for( $i=0; $i<$rows; $i++ ){
			echo "ID do Equipamento: " . $result[$i]["eq_id"] . "<br>";
			echo "Nome: " . $result[$i]["nome"] . "<br>";
			echo "Descrição: " . $result[$i]["descricao"] . "<br>";
			echo "ID do Responsável: " . $result[$i]["resp_id"] . "<br>";
			echo "ID do Local: " . $result[$i]["local_id"] . "<br><br>";
		}
	}
?>
