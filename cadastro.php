<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	
		<title>Cadastros</title>
	</head>
	
	<body>
		<?php if( $_POST['event_cad'] === 'Equipamento' ):?>
		<h1>Preencha os campos para cadastrar o equipamento</h1>
		<form action="cadastro2.php" method="post">
			<label for="eq_nome">Nome: </label>
            <br>
            <input type="text" name="eq_nome" id="nome">
            <br><br>
 
            <label for="descricao">Descricao: </label>
            <br>
            <textarea name="descricao" col="20" rows="5"></textarea>
            <br><br>
            
            <label for="projeto">Projeto: </label>
            <br>
            <input type="text" name="projeto" id="projeto">
            <br><br>
            
            <label for="resp_id">ID do responsável: </label>
            <br>
            <input type="text" name="resp_id" id="resp_id">
            <br><br>
            
            <label for="local_id">ID do local: </label>
            <br>
            <input type="text" name="local_id" id="local_id">
            <br><br>
 
            <input type="submit" value="Cadastrar">
		</form>
		
		<?php elseif( $_POST['event_cad'] === 'Local' ):?>
		<h1>Mova o marcador para escolher o local</h1>
		<div id="googleMap" style="width:100%;height:400px;"></div>
		<section>
			<div id='map_canvas'></div>
			<div id="current">Nada ainda...</div>
		</section>

		<script>
			function myMap() {
				var mapProp= {
					center:new google.maps.LatLng(-3.7387,-38.5697),
					zoom:15
				};

				var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

				var myMarker = new google.maps.Marker({
					position: new google.maps.LatLng(-3.7387,-38.5697),
					draggable: true
				});

				google.maps.event.addListener(myMarker, 'dragend', function (evt) {
					document.getElementById('current').innerHTML = "<h1>Preencha as informações</h1> <form action='cadastro2.php' method='post'> Nome:<input type='text' name='local_nome'><br> Latitude:<input type='text' name='lat' value=" + evt.latLng.lat().toFixed(5) + "><br> Longitude:<input type='text' name='lng' value=" + evt.latLng.lng().toFixed(5) + "><br> <input type='submit' value='Cadastrar local'> </form>";
				});

				map.setCenter(myMarker.position);
				myMarker.setMap(map);
			}
		</script>

		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbifWaK2G0yBo6Ga2E3a_UiUjHxsKW44o&callback=myMap"></script>
		
		<?php elseif( $_POST['event_cad'] === 'Responsável' ):?>
		<h1>Preencha os campos para cadastrar o responsável</h1>
		<form action="cadastro2.php" method="post">
			<label for="resp_nome">Nome: </label>
            <br>
            <input type="text" name="resp_nome" id="nome">
            <br><br>

            <label for="email">E-mail: </label>
            <br>
            <input type="text" name="email" id="email"> 
            <br><br>
            
            <label for="telefone">Telefone: </label>
            <br>
            <input type="text" name="telefone" id="telefone"> 
            <br><br>
 
            <input type="submit" value="Cadastrar">
		</form>
		
		<?php else:?>
			<?php header('Location: index.php'); ?>
		<?php endif; ?>
		
		<p><a href="index.php">Voltar</a></p>
	</body>
</HTML>
