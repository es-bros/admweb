<?php
	//require_once "init_emufc.php";
	
	if( isset($_POST["eq_nome"]) && isset($_POST["descricao"]) && isset($_POST["projeto"]) && isset($_POST["resp_id"]) && isset($_POST["local_id"]) ){
		$eq_nome = $_POST["eq_nome"];
		$descricao = $_POST["descricao"];
		$projeto = $_POST["projeto"];
		$resp_id = $_POST["resp_id"];
		$local_id = $_POST["local_id"];
		
		$sql_resp = "SELECT resp_id FROM responsaveis WHERE resp_id = :resp_id";
		$sql_local = "SELECT local_id FROM locais WHERE local_id = :local_id";
		
		$stmt_resp = $ePDO->prepare($sql_resp);
		$stmt_resp->bindParam(':resp_id', $resp_id);
		$stmt_resp->execute();
		$resp = $stmt_resp->fetchAll(PDO::FETCH_ASSOC);
		
		$stmt_local = $ePDO->prepare($sql_local);
		$stmt_local->bindParam(':local_id', $local_id);
		$stmt_local->execute();
		$local = $stmt_local->fetchAll(PDO::FETCH_ASSOC);
		
		if( count($resp)<=0 || count($local)<=0 ){
			echo "Erro: não há responsável ou local com essa ID";
		} else{												
			try {
				// set the PDO error mode to exception
				$ePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$insert = "INSERT INTO equipamentos (nome,descricao,projeto,resp_id,local_id) VALUES('$eq_nome','$descricao','$projeto','$resp_id','$local_id')";

				// use exec() because no results are returned
				$ePDO->exec($insert);
				echo "New record created successfully";
				header("Location: index.php");
			}
			catch(PDOException $e){
				echo $insert . "<br>" . $e->getMessage();
			}
		}
	}
	elseif( isset($_POST["resp_nome"]) && isset($_POST["email"]) && isset($_POST["telefone"]) ){
		$resp_nome = $_POST["resp_nome"];
		$email = $_POST["email"];
		$telefone = $_POST["telefone"];
		
		try {
			// set the PDO error mode to exception
			$ePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$insert = "INSERT INTO responsaveis (email,nome,telefone) VALUES('$email','$resp_nome','$telefone')";

			// use exec() because no results are returned
			$ePDO->exec($insert);
			echo "New record created successfully";
			header("Location: index.php");
		}
		catch(PDOException $e){
			echo $insert . "<br>" . $e->getMessage();
		}
	}
	elseif( isset($_POST["local_nome"]) && isset($_POST["lat"]) && isset($_POST["lng"]) ){
		$postArray = array(
		      "name" => $_POST['local_nome'],
		      "lat" => $_POST['lat'],
		      "lon" => $_POST['lng']
		    ); //you might need to process any other post fields you have..

		$json = json_encode( $postArray );
		$url = "http://localhost:4000/places/";    

		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER,
		        array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);

		$json_response = curl_exec($curl);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		if ( $status != 201 ) {
		    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
		}

		curl_close($curl);

		$response = json_decode($json_response, true); 
	}
	else{
		echo "Erro\n";
		exit;
	}
?>
