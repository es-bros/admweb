<?php
	session_start();
 
	require_once 'init.php';
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
 
        <title>Sistema de Login ULTIMATE PHP</title>
    </head>
 
    <body>
         
        <h1>Sistema de Login ULTIMATE PHP</h1>
 
        <?php if (isLoggedIn()): ?>
            <p>Olá, <?php echo $_SESSION['user_name']; ?></p>
            <h1>Cadastro</h1> 
			<form action="cadastro.php" method="post">
				<p>Cadastro de equipamento, de local e de responsável</p>
				<select name="event_cad">
					<option name="cad_equi">Equipamento</option> <!-- Done -->
					<option name="cad_local">Local</option> <!-- Done -->
					<option name="cad_resp">Responsável</option><br> <!-- Done -->
					<input type="submit" value="Cadastrar" />
				</select>
			</form>
			
			<h1>Remoção</h1>
			<form action="remove.php" method="post">
				<p>Remoção de equipamento, de local e de responsável</p>
				<select name="event_rem">
					<option name="rem_equi">Equipamento</option> <!-- Done -->
					<option name="rem_local">Local</option> <!-- Done -->
					<option name="rem_resp">Responsável</option><br> <!-- Done -->
					<input type="submit" value="Remover" />
				</select>
			</form>
			
			<h1>Atualização</h1>
			<form action="atualizar.php" method="post">
				<p>Atualização de equipamento, de local e de responsável</p>
				<select name="event_at">
					<option name="at_equi">Equipamento</option> <!-- Done -->
					<option name="at_resp">Responsável</option><br> <!-- Done -->
					<input type="submit" value="Atualizar" />
				</select>
			</form>
			
			<h1>Listar</h1>
			<form action="listar.php" method="post">
				<p>Listar equipamentos, responsáveis e equipamentos</p>
				<select name="event_list">
					<option name="list_equi">Equipamentos</option> <!-- Done -->
					<option name="list_local">Locais</option> <!-- Done -->
					<option name="list_resp">Responsáveis</option><br> <!-- Done -->
					<input type="submit" value="Listar" />
				</select>
			</form>            
        <?php else: ?>
            <p>Olá, visitante. <a href="form.php">Login</a></p>
        <?php endif; ?>
        <p><a href="logout.php">Sair</a></p>
		 
    </body>
</html>
