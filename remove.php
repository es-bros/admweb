<!DOCTYPE html>
<html>
	<head>
		<title>Remover</title>
	</head>
	
	<body>
		<?php if( $_POST["event_rem"] === "Equipamento" ): ?>
		<h1>Entre a ID do equipamento a ser removido</h1>
		<form action="remove2.php" method="post">
			<label for="id_eq">ID: </label>
            <br>
            <input type="text" name="id_eq" id="id_eq">
            <br><br>
 
            <input type="submit" value="Remover">
		</form>
		
		<?php elseif( $_POST["event_rem"] === "Local" ): ?>
		<h1>Entre a ID do local a ser removido</h1>
		<form action="remove2.php" method="post">
			<label for="id_local">ID: </label>
            <br>
            <input type="text" name="id_local" id="id_local">
            <br><br>
 
            <input type="submit" value="Remover">
		</form>
		
		<?php elseif( $_POST["event_rem"] === "Responsável" ): ?>
		<h1>Entre a ID do responsável a ser removido</h1>
		<form action="remove2.php" method="post">
			<label for="id_resp">ID: </label>
            <br>
            <input type="text" name="id_resp" id="id_resp">
            <br><br>
 
            <input type="submit" value="Remover">
		</form>
		
		<?php else: ?>
			<?php header('Location: index.php'); ?>
		<?php endif; ?>
		
		<p><a href="index.php">Voltar ao início</a></p>
	</body>
</html>
