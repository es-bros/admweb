<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	
		<title>Atualizar</title>
	</head>
	
	<body>
		<?php if( $_POST["event_at"] === "Equipamento" ): ?>
			<h1>Informe o ID do equipamento a ser atualizado</h1>
			<form action="atualizar2.php" method="post" >
				ID:<br><input name="eq_id" type="text"><br>
				<input type="submit" value="Solicitar" name="at_eq">
			</form>
			
		<?php elseif( $_POST["event_at"] === "Responsável" ): ?>
			<h1>Informe o ID do responsável a ser atualizado</h1>
			<form action="atualizar2.php" method="post" >
				ID:<br><input name="resp_id" type="text"><br>
				<input type="submit" value="Solicitar" name="at_resp">
			</form>
		
		<?php else: ?>
			<?php header('Location: index.php'); ?>
		<?php endif; ?>
		
		<p><a href="index.php">Voltar</a></p>
	</body>
</html>
