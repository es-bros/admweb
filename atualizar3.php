<?php
	require_once "init_emufc.php";

	if( isset($_POST["at_eq"]) ){
		try{
			$id = $_POST["equi_id"];
			$eq_nome = $_POST["novo_eq_nome"];
			$eq_descricao = $_POST["novo_eq_descricao"];
			$eq_resp_id = $_POST["novo_eq_resp_id"];
			$eq_local_id = $_POST["novo_eq_local_id"];
			$ePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE equipamentos SET nome='$eq_nome', descricao='$eq_descricao', resp_id='$eq_resp_id', local_id='$eq_local_id' WHERE eq_id='$id'";
			echo $sql;
			$ePDO->exec($sql);
			header("Location: index.php");
		} catch( PDOException $e ){
			echo $sql . "<br>" . $e->getMessage();
		}
	}
	elseif( isset($_POST["at_resp"]) ){
		try{
			$id = $_POST["resp_id"];
			$resp_nome = $_POST["novo_resp_nome"];
			$resp_email = $_POST["novo_resp_email"];
			$resp_tel = $_POST["novo_resp_tel"];
			$ePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE responsaveis SET email='$resp_email', nome='$resp_nome', telefone='$resp_tel' WHERE resp_id='$id'";
			echo $sql;
			$ePDO->exec($sql);
			header("Location: index.php");
		} catch( PDOException $e ){
			echo $sql . "<br>" . $e->getMessage();
		}
	}
?>
