<?php	
	require_once "init_emufc.php";
	
	if( isset($_POST["at_eq"]) ){
		$equ_id = $_POST['eq_id'];
		$sql = "SELECT * FROM equipamentos WHERE eq_id='$equ_id'";
		$stmt = $ePDO->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetch();
	}
	elseif( isset($_POST["at_resp"]) ){
		$re_id = $_POST['resp_id'];
		$sql = "SELECT * FROM responsaveis WHERE resp_id='$re_id'";
		$stmt = $ePDO->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetch();
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		
		<title>Atualizando</title>
	<head>
	
	<body>
		<h1>Entre os novos dados</h1>
		<?php if( isset($_POST["at_eq"]) ): ?>
			<form action="atualizar3.php" method="post">
				ID:<input type="text" name="equi_id" value="<?php echo $result['eq_id']; ?>" readonly><br>
				Nome:<input name="novo_eq_nome" type="text" value="<?php echo $result['nome']; ?>"><br>
				Descrição:<input name="novo_eq_descricao" type="text" value="<?php echo $result['descricao']; ?>" ><br>
				ID do Responsável:<input name="novo_eq_resp_id" type="text" value="<?php echo $result['resp_id']; ?>" ><br>
				ID do Local:<input name="novo_eq_local_id" type="text" value="<?php echo $result['local_id']; ?>" ><br>
				<input type="submit" value="Atualizar" name="at_eq">
			</form>
		
		<?php elseif( isset($_POST["at_resp"]) ): ?>
			<form action="atualizar3.php" method="post">
				ID:<input type="text" name="resp_id" value="<?php echo $result['resp_id']; ?>" readonly><br>
				Nome:<input name="novo_resp_nome" type="text" value="<?php echo $result['nome']; ?>"><br>
				E-mail:<input name="novo_resp_email" type="text" value="<?php echo $result['email']; ?>" ><br>
				Telefone:<input name="novo_resp_tel" type="text" value="<?php echo $result['telefone']; ?>" ><br>
				<input type="submit" value="Atualizar" name="at_resp">
			</form>
		<?php else: ?>
			<p>Dados entrados são inválidos</p>
		<?php endif; ?>
	</body>
</html>
