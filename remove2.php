<?php
	require_once "init_emufc.php";
	
	if( isset($_POST["id_eq"]) ){
		$equ_id = $_POST["id_eq"];
		
		$sql = "SELECT eq_id FROM equipamentos WHERE eq_id=:equ_id";
		$stmt = $ePDO->prepare($sql);
		$stmt->bindParam(':equ_id', $equ_id);
		$stmt->execute();
		$eq = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if( count($eq)<=0 ){
			echo "Esse equipamento não existe";
		} else {
			try {
				// set the PDO error mode to exception
				$ePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$del = "DELETE FROM equipamentos WHERE eq_id='$equ_id'";

				// use exec() because no results are returned
				$ePDO->exec($del);
				echo "Record deleted successfully";
				header("Location: index.php");
			}
			catch(PDOException $e){
				echo $del . "<br>" . $e->getMessage();
			}
		}
	}
	elseif( isset($_POST["id_local"]) ){
		$id_local = $_POST["id_local"];
		
		$sql = "SELECT local_id FROM locais WHERE local_id=:id_local";
		$stmt = $ePDO->prepare($sql);
		$stmt->bindParam(':local_id', $id_local);
		$stmt->execute();
		$local = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if( count($local)<=0 ){
			echo "Esse local não existe";
		} else {
			try {
				// set the PDO error mode to exception
				$ePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$del = "DELETE FROM locais WHERE local_id='$id_local'";

				// use exec() because no results are returned
				$ePDO->exec($del);
				echo "Record deleted successfully";
				header("Location: index.php");
			}
			catch(PDOException $e){
				echo $del . "<br>" . $e->getMessage();
			}
		}
	}
	elseif( isset($_POST["id_resp"]) ){
		$id_resp = $_POST["id_resp"];
		
		$sql = "SELECT resp_id FROM responsaveis WHERE resp_id=:id_resp";
		$stmt = $ePDO->prepare($sql);
		$stmt->bindParam(':id_resp', $id_resp);
		$stmt->execute();
		$resp = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if( count($resp)<=0 ){
			echo "Esse responsável não existe";
		} else {
			try {
				// set the PDO error mode to exception
				$ePDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$del = "DELETE FROM responsaveis WHERE resp_id='$id_resp'";

				// use exec() because no results are returned
				$ePDO->exec($del);
				echo "Record deleted successfully";
				header("Location: index.php");
			}
			catch(PDOException $e){
				echo $del . "<br>" . $e->getMessage();
			}
		}
	}
?>
